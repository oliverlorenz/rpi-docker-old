#!/bin/bash
set -e

sudo sed -i "s@\"dbuser\".*,\$@\"dbuser\" => \"$OWNCLOUD_DB_USER\",@g" $OWNCLOUD_CONFIG_DIR/autoconfig.php && \
sudo sed -i "s@\"dbpass\".*,\$@\"dbpass\" => \"$OWNCLOUD_DB_PASSWORD\",@g" $OWNCLOUD_CONFIG_DIR/autoconfig.php && \

exec "$@"
