#!/bin/bash

set -e

if [ -z "$OWNCLOUD_SERVERNAME" ]; then
    echo >&2 'error: you have to provide a server-name'
    echo >&2 '  Did you forget to add -e HOSTNAME=... ?'
    exit 1
fi

sudo sed -i "s/server_name localhost/server_name $OWNCLOUD_SERVERNAME/g" /etc/nginx/sites-available/default

exec "$@"
